#include <unistd.h>
#include <vector>
#include <iostream>
#include "UDPServer.h"
#include "UDPClient.h"
#include "Producer.h"
#include "Consumer.h"
#include <signal.h>
#define INVALID_SOCKET -1
#define PORT 8842
#define MIN_BUFFER_SIZE 10
#define MAX_BUFFER_SIZE 20
#define NB_MESSAGE 2

bool g_stop = false;

//generate random number between min and max
int random_interval(int min, int max)
{
  return (int)(min + ((max - min +1) * (rand () / ((double) RAND_MAX+1))));
}


void exitHandler(int s)
{
    std::cout<<"Caught signal "<<s<<std::endl;
    g_stop = true;
}

void setExitHandler()
{
    signal(SIGINT, exitHandler);
    signal(SIGILL, exitHandler);
    signal(SIGFPE, exitHandler);
    signal(SIGTERM, exitHandler);
#ifdef WIN32
    signal(SIGBREAK, exitHandler);
#endif
    signal(SIGABRT, exitHandler);

}

int main()
{

    //Create the UDP server
    std::unique_ptr<UDPServer> udpServer  = std::unique_ptr<UDPServer>(new UDPServer(PORT));
    std::string error;
    udpServer->getError(error);
    if(!error.empty())
    {
      printf("Could not initialise UDP Server\n");
      return 1;
    }
    getSocketInfo();
    if(udpServer->setBlockingSocket(false))
    {
        printf("Could not activated non blocking mode \n");
        return 1;
    }

    //create UDP client
    char host[256];
    int hostname;
    hostname = gethostname(host, sizeof(host));
    if(hostname == -1)
    {
      printf("Could not find hostname\n");
       return 1;
    }


    std::shared_ptr<UDPClient> udpClient(new UDPClient(PORT,host));
    udpClient->getError(error);
    if(!error.empty())
    {
      printf("Could not initialise UDP client\n");
      return 1;
    }

    std::vector<size_t> protocolDataSize;
    protocolDataSize.reserve(NB_MESSAGE);
    protocolDataSize.push_back(random_interval(MIN_BUFFER_SIZE, MAX_BUFFER_SIZE));
    protocolDataSize.push_back(random_interval(MIN_BUFFER_SIZE, MAX_BUFFER_SIZE));
    Producer producer(protocolDataSize,udpClient);
    Consumer consumer(protocolDataSize,std::move(udpServer));
    setExitHandler();
    producer.product();
    while (!g_stop) {
        consumer.consume();
    }
    producer.stop();
    consumer.stop();
return 0;
}
