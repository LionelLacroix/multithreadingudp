#pragma once
#include <sys/types.h> //bind
#include <sys/socket.h> //udp/socket bind
#include <string>

void getSocketInfo();

class UDPServer
{

public:
    UDPServer(int port);
    ~UDPServer();
    void getError(std::string& error) { error = m_error;}
    int setBlockingSocket(bool IsBlocking);
    int receive(void* buffer, int size);

private:
    int m_socketID;
    struct sockaddr m_clientAdresses;
    std::string m_error;
};
