#include "ProductMsg.h"
#include <iostream>
#define MIN_BUFFER_SIZE 1
#define MAX_BUFFER_SIZE 30

#include <fstream>

//generate random number between min and max
double random_double_interval(int min, int max)
{
  return (double)(min + ((max - min +1) * (rand () / ((double) RAND_MAX+1))));
}


ProductMsg::ProductMsg(std::shared_ptr<UDPClient> & client,size_t frequency ,size_t type, size_t dataSize):
    m_isready(false),
    m_stop(false),
    m_type(type),
    m_UDPClient(client),
    m_frequency(frequency),
    m_thread(std::thread([this](){ run();}))
{

    m_header.size = static_cast<int>(dataSize);
    m_header.type = static_cast<int>(type);
    m_data.resize(dataSize);
    m_buffer = std::unique_ptr<char[]>(new char[sizeof(m_header)+dataSize*sizeof(double)]);
}


void ProductMsg::run()
{
    while(true)
    {
        std::unique_lock lock(m_mutex);
        m_cv.wait(lock, [this]() { return m_isready;});
        if (m_stop && m_isready )
            return;
        updateData();
        *(HeaderMsg*)&m_buffer[0] = m_header;
        for(int i = 0; i <m_header.size ;++i)
            *(double*)&m_buffer[sizeof(m_header)+i*sizeof(double)] = m_data[i];


        int nbBytes = m_UDPClient->UDPSend(m_buffer.get(),sizeof(m_header)+m_header.size*sizeof(double));
        if(nbBytes <=0)
        {
            m_stop =true;
            m_error = "Could not send udp Message";
        }
       std::this_thread::sleep_for(std::chrono::milliseconds(1000/m_frequency));
    }
}

 void ProductMsg::showData()
 {
    for(size_t i =0; i< m_data.size(); ++i)
        std::cout<<m_data[i]<<" "<<std::flush;
    std::cout<<std::flush<<std::endl;
 }


void ProductMsg::start()
{
    std::unique_lock lock(m_mutex);
    m_isready = true;
    m_cv.notify_one();
}

void ProductMsg::updateData()
{
    for(size_t i =0; i< m_data.size();++i)
       m_data[i] = (double)random_double_interval(MIN_BUFFER_SIZE,MAX_BUFFER_SIZE);
}


void ProductMsg::stop()
{
    m_stop =true;
    if(m_thread.joinable())
        m_thread.join();
}

