#include "Producer.h"
#include <functional>
#include <iostream>

Producer::Producer(const std::vector<size_t> & protocolDataSize,
                   std::shared_ptr<UDPClient>& UDPClient)
{
    for(size_t i = 0; i < protocolDataSize.size(); ++i)
       m_ProductMsgs.push_back(std::unique_ptr<ProductMsg>(new ProductMsg(UDPClient,20,i,protocolDataSize[i])));
}

void Producer::product()
{
    for(auto & productMsg  : m_ProductMsgs)
    {
        productMsg->start();
        if(productMsg->isStopped())
            std::cerr<<productMsg->getError()<<std::endl;
    }
}
void Producer::stop()
{
    for(auto & productMsg  : m_ProductMsgs)
        productMsg->stop();
}

