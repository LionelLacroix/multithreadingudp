#include <vector>
#include "ProductMsg.h"
#include <memory>

class Producer
{
public:
    explicit Producer(const std::vector<size_t> & protocolDataSize,
                      std::shared_ptr<UDPClient>& UDPClient );
    void product();
    void stop();
    ~Producer() =default;
private:
    std::vector<std::unique_ptr<ProductMsg>> m_ProductMsgs;
    std::unique_ptr<UDPClient> m_UDPClient;
};
