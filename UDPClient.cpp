#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

#include "UDPClient.h"
#include <unistd.h> //close
#include <netinet/in.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <time.h>
#include <ifaddrs.h>
#include <sys/types.h> //bind
#include <sys/socket.h> //udp/socket bind

#define INVALID_SOCKET -1


//brief intialize the UDP client
UDPClient::UDPClient(int port, const char *serverName):m_socketID(INVALID_SOCKET)
 {
  m_socketID = socket(AF_INET, SOCK_DGRAM, 0);
  if (INVALID_SOCKET == m_socketID)
    printf(" Could not open socket\n");

  struct hostent * mmserv = gethostbyname(serverName);
  if(mmserv == 0)
    printf("Could not find host \" %s \" \n",serverName );
  else
    printf("Successfullly opened socket for UDP client on server %s\n", serverName);

  struct sockaddr_in name;
  memcpy((char*)(&(name.sin_addr.s_addr)), mmserv->h_addr, mmserv->h_length);
  name.sin_family = AF_INET;
  name.sin_port = htons(port);
  int ret = connect(m_socketID, (struct sockaddr*)&name, sizeof(name));
  if(ret!= 0)
    printf("Could connect sockerd ID");
 }

//brief send the UDP message form socket id
int UDPClient::UDPSend( void *buffer, int size)
{
  if (m_socketID == INVALID_SOCKET)
        return INVALID_SOCKET;
  int sentBytes = send(m_socketID, (char*)buffer, size, 0);
  return sentBytes;
}

UDPClient::~UDPClient()
{
    close(m_socketID);
}
