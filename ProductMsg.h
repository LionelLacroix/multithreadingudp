#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <string>
#include <functional>
#include <future>
#include <vector>
#include <thread>
#include <memory>
#include "UDPClient.h"
#include "Protocol.h"

class ProductMsg
{
public:
    explicit ProductMsg(std::shared_ptr<UDPClient> & client, size_t frequency, size_t type, size_t dataSize);
    ProductMsg(const ProductMsg &) = delete;
    ProductMsg(ProductMsg &&) = delete;
    ProductMsg &operator=(const ProductMsg &) = delete;
    ProductMsg &operator=(ProductMsg &&) = delete;
    ~ProductMsg() =default;
    void stop();
    std::string getError() {return m_error;}
    bool isStopped() {return m_stop;}
    void start();
private:
    void run();
    void updateData();
    void showData();

private:

    bool m_isready;
    bool m_stop;
    int m_type;
    mutable std::mutex m_mutex;
    std::condition_variable m_cv;
    std::shared_ptr<UDPClient> m_UDPClient;
    std::vector<double> m_data;
    std::unique_ptr<char[]> m_buffer;
    int m_frequency;
    HeaderMsg m_header;
    std::thread m_thread;
    std::string m_error;

};
