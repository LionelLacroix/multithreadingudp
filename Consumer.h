#include <vector>
#include "UDPServer.h"
#include "ThreadPool.h"
#include "Protocol.h"
#include <memory>
#include <unordered_map>

class Consumer
{
public:
    explicit Consumer(const std::vector<size_t> & protocolDataSize, std::unique_ptr<UDPServer>&& server);

    void consume();
    void stop();
    ~Consumer() =default;
private:
    std::unordered_map<size_t,std::unique_ptr<double[]>> m_protocolData;
    std::unique_ptr<UDPServer> m_UDPServer;
    std::unique_ptr<ThreadPool> m_threadPool;
    std::unique_ptr<char[]> m_rcvBuffer;
    size_t m_bufferSize;
};
