#pragma once
#include <string>

class UDPClient
{
public:
    UDPClient(int port, const char *serverName);
    int UDPSend(void *buffer, int size);
    void getError(std::string & error) {error =  m_error;};
    ~UDPClient();
private:
    int m_socketID;
    std::string m_error;
};

