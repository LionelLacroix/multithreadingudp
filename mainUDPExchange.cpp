
#include <ev.h>
#include <unistd.h>
#include <pthread.h>
#include <netdb.h>
#include "UDPServer.h"
#include "UDPClient.h"
#define PORT 8842
#define MIN_BUFFER_SIZE 10
#define MAX_BUFFER_SIZE 20
#define INVALID_SOCKET -1


pthread_mutex_t lock;
int expectedBufferSize = 0;
char sendBuffer[sizeof(int)*MAX_BUFFER_SIZE];
char rcvBuffer[sizeof(int)*MAX_BUFFER_SIZE];
struct ev_loop* loopThread;
struct ev_async async_w;
UDPServer* server = nullptr;
UDPClient* client = nullptr;


void showBuf(int* buffer, int size)
{
  int i =0;
  for(i =0; i< size; i= i+1)
  {
     printf("%d ", buffer[i]);
  }
  printf("\n");
}



//generate random number between min and max
int random_interval(int min, int max)
{
  return (int)(min + ((max - min +1) * (rand () / ((double) RAND_MAX+1))));
}



//fill buffer randomly
void generateBuffer(int* buffer, int size)
{
  int i = 0;
  for(i =0; i< size; i= i+1)
  {
     buffer[i] = (int)rand();
  }
}

//brief show all values of the buffer

void tri(int *buffer, int N)
{
  int i,j,c = 0;
  for(i=0;i<N-1;i++)
  {
    for(j=i+1;j<N;j++)
    {
      if ( buffer[i] > buffer[j] )
      {
        c = buffer[i];
        buffer[i] = buffer[j];
        buffer[j] = c;
      }
    }
  }
}

//brief receive UDP message from UDP client
static void receive_udp_msg(struct ev_loop*,  ev_async *, int ) {

  memset(rcvBuffer,0,sizeof(rcvBuffer));
  int rcvSize =0;
  int bufferSize = expectedBufferSize/sizeof(int);
   rcvSize  = expectedBufferSize = server->receive(rcvBuffer,expectedBufferSize);
  if(rcvSize>0)
  {
    tri((int*)&rcvBuffer[0],bufferSize);
    printf("Buffer received sorted : ");
    showBuf((int*)&rcvBuffer[0],bufferSize);
    printf("\n");
  }

}

//brief fill UDP message send it to the UDP server
static void send_udp_msg(struct ev_loop*,  ev_periodic *, int ) {
    int bufferSize = random_interval(MIN_BUFFER_SIZE, MAX_BUFFER_SIZE);
    generateBuffer((int*)&sendBuffer[0], bufferSize);
    printf("\nBuffer sent : ");
    showBuf((int*)&sendBuffer[0],bufferSize);
    expectedBufferSize = client->UDPSend(sendBuffer,bufferSize*sizeof(int));
    if(expectedBufferSize >0)
    {
        ev_async_send (loopThread, &async_w);
    }
}



//brief thread function to runthe server in a new thread
void* loopThreadFunc(void* )
{
    ev_loop(loopThread, 0);
    return NULL;
}

int main()
{

    //Create the server
     server = new UDPServer(PORT);
    std::string error;
    server->getError(error);
    if(!error.empty())
    {
      printf("Could not initialise UDP Server\n");
      return 1;
    }
    getSocketInfo();
    if(server->setBlockingSocket(false))
    {
        printf("Could nit activated non blocking mode \n");
        return 1;
    }

    loopThread = ev_loop_new(0);
    //Initialize pthread
    pthread_mutex_init(&lock, NULL);
    pthread_t thread;
    ev_async_init(&async_w, receive_udp_msg);
    ev_async_start(loopThread, &async_w);
    pthread_create(&thread, NULL, loopThreadFunc, NULL);

  //create client
  char host[256];
  int hostname;
  hostname = gethostname(host, sizeof(host));
  if(hostname == -1)
  {
    printf("Could not find hostname\n");
     return 1;
  }

  client = new UDPClient(PORT,host);
  client->getError(error);
  if(!error.empty())
  {
    printf("Could not initialise UDP client\n");
    return 1;
  }

  struct ev_loop* dyn_loop;
  dyn_loop = ev_loop_new(0);
  struct ev_periodic peridic_w;
  ev_periodic_init(&peridic_w,send_udp_msg,0. ,5.,0);
  ev_periodic_start(dyn_loop,&peridic_w);
  // now wait for events to arrive
  ev_loop(dyn_loop, 0);
  //Wait on threads for execution
  pthread_join(thread, NULL);
  pthread_mutex_destroy(&lock);
  return 0;
}
