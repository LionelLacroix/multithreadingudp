#include "Consumer.h"
#include <functional>
#include <numeric>
#include <cstring>
#include <iostream>
#include "Protocol.h"
#define NBR_MAX_THREAD 4


void showBuffer(const double* buffer, size_t size)
{
  for(size_t i=0;i<size;i++)
     printf("%f ", buffer[i]);
  printf("\n");
}

void tri(double* buffer, size_t size)
{
  double c = 0;
  for(size_t i=0;i<size;i++)
  {
    for(size_t j=i+1;j<size;j++)
    {
      if ( buffer[i] > buffer[j] )
      {
        c = buffer[i];
        buffer[i] = buffer[j];
        buffer[j] = c;
      }
    }
  }
}

Consumer::Consumer(const std::vector<size_t> & protocolDataSize, std::unique_ptr<UDPServer>&& UDPServer):
    m_UDPServer(std::move(UDPServer)),
    m_bufferSize(0)
{
   size_t max = 0;
   for(size_t iType=0 ; iType < protocolDataSize.size(); ++iType)
   {
       m_protocolData[iType]= std::unique_ptr<double[]>(new double[protocolDataSize[iType]]);
       if(max < protocolDataSize[iType])
           max =protocolDataSize[iType];
   }


   m_bufferSize = max;
   m_rcvBuffer = std::unique_ptr<char[]>(new char[sizeof(HeaderMsg)+ m_bufferSize*sizeof(double)]);
   size_t min  = std::min<size_t>( protocolDataSize.size(), NBR_MAX_THREAD);
   m_threadPool = std::unique_ptr<ThreadPool>(new ThreadPool(min));
}

void Consumer::consume()
{
    int  sizeHeaderMsg = sizeof(HeaderMsg);
    std::memset(m_rcvBuffer.get(),0,sizeHeaderMsg+m_bufferSize*sizeof(double));
    int nbBytes = m_UDPServer->receive(m_rcvBuffer.get(),sizeHeaderMsg+m_bufferSize*sizeof(double));
    //first value is the type of value
    if(nbBytes> 0)
    {
        HeaderMsg* header  = (HeaderMsg*)&m_rcvBuffer.get()[0];
        auto it = m_protocolData.find(header->type);
        if(it != m_protocolData.end())
        {
            auto *data = it->second.get();
            int size = header->size;
            for(int i= 0; i<header->size; ++i )
                data[i] = *(double*)&m_rcvBuffer.get()[sizeHeaderMsg+i*sizeof(double)];

            m_threadPool->enqueue([data , size]
            {
                if(data)
                {
                    tri(data,size);
                    printf("Buffer received sorted : ");
                    showBuffer(data,size);
                    printf("\n");
                }
            });
        }
        else
        {
            std::cerr<<" Could not retrieve message"<<std::endl;
            return;
        }
    }
}

void Consumer::stop()
{
    m_threadPool->stopThread();
}

