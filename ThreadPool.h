#include <string>
#include <functional>
#include <future>
#include <queue>
#include <vector>
#include <thread>

class ThreadPool
{
  public:
    ThreadPool(const ThreadPool &) = delete;
    ThreadPool(ThreadPool &&) = delete;
    ThreadPool &operator=(const ThreadPool &) = delete;
    ThreadPool &operator=(ThreadPool &&) = delete;

    ThreadPool(size_t nr_threads);
    void stopThread();

    ~ThreadPool() =default;

    template <class F, class... Args>
    std::future<std::result_of_t<F(Args...)>> enqueue(F &&f, Args &&...args)
    {/* The return type of task `F` */
       using return_type = std::result_of_t<F(Args...)>;

       /* wrapper for no arguments */
       auto task = std::make_shared<std::packaged_task<return_type()>>(
           std::bind(std::forward<F>(f), std::forward<Args>(args)...));

       std::future<return_type> res = task->get_future();
       {
           std::unique_lock lock(mtx);

           if (m_stop)
               throw std::runtime_error("The thread pool has been stop.");

           /* wrapper for no returned value */
           tasks.emplace([task]() -> void { (*task)(); });
       }
       cv.notify_one();
       return res;
    }

  private:
    std::vector<std::thread> workers;
    std::queue<std::function<void()>> tasks;

    /* For sync usage, protect the `tasks` queue and `stop` flag. */
    std::mutex mtx;
    std::condition_variable cv;
    bool m_stop;
};
