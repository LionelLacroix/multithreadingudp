#include "ThreadPool.h"

ThreadPool::ThreadPool(size_t nr_threads):m_stop(false)
{
    for (size_t i = 0; i < nr_threads; ++i)
    {
        std::thread worker([this]() {
            while (true)
            {
                std::function<void()> task;
                {
                    std::unique_lock lock(mtx);
                    cv.wait(lock, [this]() { return m_stop || !tasks.empty(); });
                    if (m_stop && tasks.empty())
                        return;
                    task = std::move(tasks.front());
                    tasks.pop();
                }
                task();
            }
        });
        workers.emplace_back(std::move(worker));
    }
}

void ThreadPool::stopThread()
{
    /* stop thread pool, and notify all threads to finish the remained tasks. */
     {
         std::unique_lock<std::mutex> lock(mtx);
         m_stop = true;
     }
     cv.notify_all();
     for (auto &worker : workers)
        if(worker.joinable())
            worker.join();
}

