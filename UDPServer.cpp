#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include "UDPServer.h"
#include <unistd.h> //close
#include <netinet/in.h>
 #include <netdb.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <time.h>
#include <ifaddrs.h>
#define INVALID_SOCKET -1



//show all ipv4 of network interface listening by the server
void getSocketInfo()
{
    printf("Listening on the following IP Adresses:\n");

    struct ifaddrs * ifAddrStruct = NULL, * ifa = NULL;
    void * tmpAddrPtr = NULL;
    getifaddrs(&ifAddrStruct);
    for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa ->ifa_addr->sa_family == AF_INET)
        {
            tmpAddrPtr = &((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            printf("IP Address %s\n",addressBuffer);
        }
    }
    if (ifAddrStruct != NULL)
        freeifaddrs(ifAddrStruct);
}

//brief show all values of the buffer
void showBuffer(int* buffer, int size)
{
  int i =0;
  for(i =0; i< size; i= i+1)
  {
     printf("%d ", buffer[i]);
  }
  printf("\n");
}

UDPServer::UDPServer(int port):m_socketID(INVALID_SOCKET)
{
  m_socketID = socket(AF_INET, SOCK_DGRAM, 0);
  if (INVALID_SOCKET == m_socketID)
        printf("Could not open socket");
  struct sockaddr_in name;
  name.sin_family = AF_INET;
  name.sin_port = htons(port);
  name.sin_addr.s_addr = htonl(INADDR_ANY);
  struct sockaddr* nameSock = (struct sockaddr*)&name;
  int ret = bind(m_socketID, nameSock, sizeof(name));
  if (ret!=0)
      printf("Could not open port number  %d\n", port);

  int intopt = 80000;
  ret = setsockopt(m_socketID, SOL_SOCKET, SO_RCVBUF, (char*)&intopt, sizeof(int));
  if(ret != 0)
      printf("Could not set option to the socket");
  printf("Successfullly opened socket on the server \n");
}

int UDPServer::setBlockingSocket(bool IsBlocking)
{
    int ret = 0;
    if (IsBlocking)
    {
        int flags = 0;
        ret = ioctl(m_socketID, FIONBIO, &flags);
    }
    else
    {
        int flags = 1;
        ret = ioctl(m_socketID, FIONBIO, &flags);
    }
    return ret;
}


//brief UDP server receive data
int UDPServer::receive(void* buffer, int size)
{
    if (m_socketID == INVALID_SOCKET)
        return INVALID_SOCKET;
    struct sockaddr from;
    socklen_t fromlen = sizeof(from);
    int received_bytes = recvfrom(m_socketID,  (char*)buffer, size, 0, &from, (socklen_t*)&fromlen);

    if (received_bytes>0)
        m_clientAdresses = from;
    return received_bytes;
}



//brief free socket id
UDPServer::~UDPServer( )
{
  close(m_socketID);
}



